# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

HISTCONTROL=ignoreboth

# Setup ibus
export GTK_IM_MODULE=ibus
export XMODIFIERS=@im=ibus
export QT_IM_MODULE=ibus

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=
stty -ixon

# Stop git from using a popup menu when prompting for password
unset SSH_ASKPASS

# User specific aliases and functions

# Use extended vim
alias vim='vimx'

# Create .tar.xz archive
alias xzarchive='tar cfJ'

# Automatically recompile LaTeX file on save
alias latexwatch="latexmk -pdf -pvc"

# Compile jekyll site.
alias jekyllserve="bundle exec jekyll serve"

# Compile and watch Sass files.
alias sasswatch='sass --watch --style compressed'

# Encrypt PDF
encryptpdf ()
{
	qpdf --encrypt "$1" "$1" 256 -- "$2" "$3"
}

# Plugins
if [ -f `which powerline-daemon` ]; then
	powerline-daemon -q
	POWERLINE_BASH_CONTINUATION=1
	POWERLINE_BASH_SELECT=1
	. /usr/share/powerline/bash/powerline.sh
fi

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/usr/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/usr/etc/profile.d/conda.sh" ]; then
        . "/usr/etc/profile.d/conda.sh"
    else
        export PATH="/usr/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

