" Don't do highlighting on Ruby code for Jekyll.
syntax region markdownIgnore start="{% [^(end)].* %}" end="{% end.* %}"
syntax region markdownIgnore start="\$\$" end="\$\$"

" Don't try to do syntax highlighting of LaTeX inside of Markdown files.
syntax region markdownIgnore start="\$" end="\$"
